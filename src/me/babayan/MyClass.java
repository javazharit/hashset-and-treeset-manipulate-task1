package me.babayan;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class MyClass {
    public static void main(String[] args) {
        int inputNumber = 0;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please input integer number.  ");
        try {
            System.out.print("You entered number: ");
            inputNumber = Integer.parseInt(br.readLine());
        } catch (Exception ex) {
            System.out.println("Sorry, invalid input format.");

        }

        System.out.println("=========================================================================================");
        System.out.println("\n" + "HashSet CRUD time testing...");

        Set<Integer> hashSet = new HashSet<>(inputNumber);
        long startTime = System.currentTimeMillis();

        //add time
        for (int i = 0; i < inputNumber; i++) {
            hashSet.add(i);
        }
        System.out.println("Add time(ms): " + (System.currentTimeMillis() - startTime));

        //read time
        for (Integer i : hashSet){
            System.out.print("");
        }
        System.out.println("Read time(ms): " + (System.currentTimeMillis() - startTime));

        //update time
        for (int i = 0; i < inputNumber; i++) {
            hashSet.add(i);
        }
        System.out.println("Update time(ms): " + (System.currentTimeMillis() - startTime));

        //delete time
        for (int i = 0; i < inputNumber; i++) {
            hashSet.remove(i);
        }
        System.out.println("Delete time(ms): " + (System.currentTimeMillis() - startTime));


        System.out.println("=========================================================================================");
        System.out.println("\n" + "TreeSet CRUD time testing...");

        Set<Integer> treeSet = new TreeSet<>();
        long startTime1 = System.currentTimeMillis();

        //add time
        for (int i = 0; i < inputNumber; i++) {
            treeSet.add(i);
        }
        System.out.println("Add time(ms): " + (System.currentTimeMillis() - startTime1));

        //read time
        for (Integer i : treeSet){
            System.out.print("");
        }
        System.out.println("Read time(ms): " + (System.currentTimeMillis() - startTime1));

        //update time
        for (int i = 0; i < inputNumber; i++) {
            treeSet.add(i);
        }
        System.out.println("Update time(ms): " + (System.currentTimeMillis() - startTime1));

        //delete time
        for (int i = 0; i < inputNumber; i++) {
            treeSet.remove(i);
        }
        System.out.println("Delete time(ms): " + (System.currentTimeMillis() - startTime1));



    }
}
